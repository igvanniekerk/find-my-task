require('dotenv').config()
const usersSeed = require('./app/config/userSeeder.js')
const taskSeeder = require('./app/config/taskSeeder.js')
const cronJob = require('./app/config/cronUtils.js')
const logger = require('./app/config/loggerUtils')
const Database = require( './app/config/mongoUtils.js' );
const express = require('express')
const cron = require('node-cron')
/**
 * Connects to the db and seeds it
 */
Database.getDb().then(async (db) => {
  await db.collection('users').drop();
  const usersCollection = db.collection('users');
  const result = await usersCollection.insertMany(usersSeed);
  logger.info(`Users Seeded with ${result}`)
  await db.collection('tasks').drop();
  const tasksCollection = db.collection('tasks');
  const taskResult = await tasksCollection.insertMany(taskSeeder);
  logger.info(`Tasks Seeded with ${taskResult}`)
})

/**
 * Load Routes
 */
const tasksRoutes = require('./app/routes/tasks.js')
const usersRoutes = require('./app/routes/users.js')

const app = express();
app.use(express.json());
app.use('/', usersRoutes);
app.use('/', tasksRoutes);
app.listen(process.env.PORT, () => console.log(`Server running on port: http://localhost:${process.env.PORT || 5000}`));


// Schedule a job to run every minute
cron.schedule('* * * * *', () => {
  cronJob()
});

