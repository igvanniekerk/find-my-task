const { Router } = require('express')
const router = Router()
const Task = require('../controllers/tasks')
const logger = require('../config/loggerUtils')

/**
 * Method: POST
 * Function: Create Task
 * Params: user_id
 * Return: New Task
 */
router.post('/api/users/:user_id/tasks', async (req, res) => {
  try {
    const newTask = { ...req.body, user_id: req.params.id }
    const tasks = await Task.create(newTask)
    console.log(tasks)
    res.json(tasks)
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: 'An error occurred while fetching tasks' })
  }
})

/**
 * Method: PUT
 * Function: Update Task
 * Params: user_id, task_id
 * Return: Updated Task
 */
router.put('/api/users/:user_id/tasks/:task_id', async (req, res) => {
  try {
    const user_id = req.params.user_id
    const task_id = req.params.task_id

    const tasks = await Task.update(task_id, req.body)
    logger.info(`Task Updated:  ${tasks}`)
    res.status(200).json({ status: 'success', data: tasks })
  } catch (error) {
    logger.error(`Error Updating Task: ${error}`)
    res.status(500).json({ status: 'fail', error: `Error Updating Task: ${error}` })
  }
})

//TODO handle if user id does not match and check that both is is present
/**
 * Method: DELETE
 * Function: Delete Task
 * Params: user_id, task_id
 * Return: if the task is deleted
 */
router.delete('/api/users/:user_id/tasks/:task_id', async (req, res) => {
  try {
    const user_id = req.params.user_id
    const task_id = req.params.task_id

    const tasks = await Task.delete(user_id, task_id)

    if (tasks.deletedCount) {
      logger.info(`Task Deleted:  ${tasks}`)
      res.status(200).json({ status: 'success', data: tasks })
    } else {
      logger.error(`Task Does not exist:  ${task_id}`)
      res.status(500).json({ status: 'fail', error: `Error Deleting Task: Task Does not exist` })
    }
  } catch (error) {
    logger.error(`Error Deleting Task: ${error}`)
    res.status(500).json({ status: 'fail', error: `Error Deleting Task: ${error}` })
  }
})

/**
 * Method: GET
 * Function: Get Task
 * Params: user_id, task_id  
 * Return: Task
 */
router.get('/api/users/:user_id/tasks/:task_id', async (req, res) => {
  try {
    const user_id = req.params.user_id
    const task_id = req.params.task_id

    const task = await Task.getById(task_id)
    logger.info(`Task found:  ${task}`)
    res.status(200).json({ status: 'success', data: task })
  } catch (error) {
    logger.error(`Error Getting Task: ${error}`)
    res.status(500).json({ status: 'fail', error: `Error Getting Task: ${error}` })
  }
})

/**
 * Method: GET
 * Function: Get All Tasks for user
 * Params: user_id
 * Return: Tasks
 */
router.get('/api/users/:user_id/tasks/', async (req, res) => {
  try {
    const user_id = req.params.user_id

    const task = await Task.getAllForUser(user_id)
    logger.info(`Task found:  ${task}`)
    res.status(200).json({ status: 'success', data: task })
  } catch (error) {
    logger.error(`Error Getting Task: ${error}`)
    res.status(500).json({ status: 'fail', error: `Error Getting Task: ${error}` })
  }
})

module.exports = router
