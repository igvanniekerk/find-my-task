const express = require('express')
const router = express.Router()
const User = require('../controllers/users.js')
const logger = require('../config/loggerUtils')

/**
 * Method: POST
 * Function: Create User
 * Return: New User
 */
router.post('/api/users', async (req, res) => {
  try {
    const users = await User.create(req.body)
    logger.info(`User Created:  ${users}`)
    res.status(200).json({status: 'success' , data: users})
  } catch (error) {
    logger.error(`Error Creating User: ${error}`)
    res.status(500).json({status: 'fail', error })
  }
})

/**
 * Method: PUT
 * Function: Update User
 * Params: user_id
 * Return: Updated User
 */
router.post('/api/users/:id', async (req, res) => {
  try {
    const users = await User.update(req.params.id, req.body)
    logger.info(`User Updated:  ${users}`)
    res.status(200).json({status: 'success' , data: users})
  } catch (error) {
    logger.error(`Error Updating User: ${error}`)
    res.status(500).json({status: 'fail', error: `Error Updating User: ${error}`})
  }
})

/**
 * Method: GET
 * Function: Get ALL User
 * Return: All User
 */
router.get('/api/users', async (req, res) => {
  try {
    const users = await User.listAll()
    logger.info(`User List:  ${users}`)
    res.status(200).json({status: 'success' , data: users})
  } catch (error) {
    logger.error(`Error getting all Users: ${error}`)
    res.status(500).json({status: 'fail', error: `Error getting all User: ${error}`})
  }
})

/**
 * Method: GET
 * Function: Get User
 * Params: user_id
 * Return: User
 */
router.get('/api/users/:id', async (req, res) => {
  try {
    const user = await User.getById(req.params.id)
    res.status(200).json({status: 'success' , data: users})
  } catch (error) {
    logger.error(`Error getting Users: ${error}`)
    res.status(500).json({status: 'fail', error: `Error getting User: ${error}`})
  }
})

module.exports = router
