const { ObjectId } = require( 'mongodb' );

/**
 * Task defualt Seeder JSON
 */
const tasksSeed = [
    {
        "_id": new ObjectId("662ff7e536662d902a753c01"),
        "name": "Code Review",
        "description": "Review the latest pull requests",
        "date_time": "2024-05-10 10:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-10 10:00:00",
        "user_id": new ObjectId("663741342683274b18c677a3")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c02"),
        "name": "Team Meeting",
        "description": "Attend the weekly team meeting",
        "date_time": "2024-05-11 14:00:00",
        "status": "completed",
        "next_execute_date_time": "2024-05-11 14:00:00",
        "user_id": new ObjectId("663741342683274b18c677a3")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c03"),
        "name": "Debug Issue",
        "description": "Fix a critical software bug",
        "date_time": "2024-05-12 18:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-12 18:00:00",
        "user_id": new ObjectId("663741342683274b18c677a3")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c04"),
        "name": "Hiking Trip",
        "description": "Go on a weekend hiking trip",
        "date_time": "2024-05-15 08:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-15 08:00:00",
        "user_id": new ObjectId("663741342683274b18c677a4")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c05"),
        "name": "Plant New Flowers",
        "description": "Plant flowers in the backyard",
        "date_time": "2024-05-13 16:00:00",
        "status": "completed",
        "next_execute_date_time": "2024-05-13 16:00:00",
        "user_id": new ObjectId("663741342683274b18c677a4")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c06"),
        "name": "Nature Photography",
        "description": "Take photos in the forest",
        "date_time": "2024-05-17 10:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-17 10:00:00",
        "user_id": new ObjectId("663741342683274b18c677a4")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c07"),
        "name": "Read New Book",
        "description": "Read 'Atomic Habits'",
        "date_time": "2024-05-12 19:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-12 19:00:00",
        "user_id": new ObjectId("663741342683274b18c677a5")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c08"),
        "name": "Write a Blog Post",
        "description": "Write a review of a recent book",
        "date_time": "2024-05-16 12:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-16 12:00:00",
        "user_id": new ObjectId("663741342683274b18c677a5")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c09"),
        "name": "Book Club Meeting",
        "description": "Join the monthly book club meeting",
        "date_time": "2024-05-18 18:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-18 18:00:00",
        "user_id": new ObjectId("663741342683274b18c677a5")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c10"),
        "name": "Gym Workout",
        "description": "Complete the workout routine",
        "date_time": "2024-05-11 06:30:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-11 06:30:00",
        "user_id": new ObjectId("663741342683274b18c677a6")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c11"),
        "name": "Buy New Equipment",
        "description": "Get new fitness gear",
        "date_time": "2024-05-13 13:00:00",
        "status": "completed",
        "next_execute_date_time": "2024-05-13 13:00:00",
        "user_id": new ObjectId("663741342683274b18c677a6")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c12"),
        "name": "Yoga Session",
        "description": "Attend a yoga class",
        "date_time": "2024-05-19 07:30:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-19 07:30:00",
        "user_id": new ObjectId("663741342683274b18c677a6")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c13"),
        "name": "Art Gallery Visit",
        "description": "Visit the local art gallery",
        "date_time": "2024-05-16 15:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-16 15:00:00",
        "user_id": new ObjectId("663741342683274b18c677a7")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c14"),
        "name": "Sketch New Designs",
        "description": "Create sketches for a new painting",
        "date_time": "2024-05-14 18:00:00",
        "status": "completed",
        "next_execute_date_time": "2024-05-14 18:00:00",
        "user_id": new ObjectId("663741342683274b18c677a7")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c15"),
        "name": "Art Workshop",
        "description": "Attend a pottery workshop",
        "date_time": "2024-05-18 11:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-18 11:00:00",
        "user_id": new ObjectId("663741342683274b18c677a7")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c16"),
        "name": "Music Practice",
        "description": "Practice the guitar for 1 hour",
        "date_time": "2024-05-14 17:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-14 17:00:00",
        "user_id": new ObjectId("663741342683274b18c677a8")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c17"),
        "name": "Buy New Music Gear",
        "description": "Purchase a new amplifier",
        "date_time": "2024-05-12 13:00:00",
        "status": "completed",
        "next_execute_date_time": "2024-05-12 13:00:00",
        "user_id": new ObjectId("663741342683274b18c677a8")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c18"),
        "name": "Attend a Concert",
        "description": "Go to a live music concert",
        "date_time": "2024-05-19 20:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-19 20:00:00",
        "user_id": new ObjectId("663741342683274b18c677a8")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c19"),
        "name": "Game Tournament",
        "description": "Compete in a gaming tournament",
        "date_time": "2024-05-18 20:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-18 20:00:00",
        "user_id": new ObjectId("663741342683274b18c677a9")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c20"),
        "name": "Watch a Game Review",
        "description": "Watch a YouTube game review",
        "date_time": "2024-05-15 16:00:00",
        "status": "completed",
        "next_execute_date_time": "2024-05-15 16:00:00",
        "user_id": new ObjectId("663741342683274b18c677a9")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c21"),
        "name": "Update Gaming Setup",
        "description": "Upgrade the gaming PC",
        "date_time": "2024-05-20 14:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-20 14:00:00",
        "user_id": new ObjectId("663741342683274b18c677a9")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c22"),
        "name": "Try New Recipe",
        "description": "Cook a new dish",
        "date_time": "2024-05-13 12:30:00",
        "status": "pending",
        "next_execute date_time": "2024-05-13 12:30:00",
        "user_id": new ObjectId("663741342683274b18c677aa")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c23"),
        "name": "Food Tasting",
        "description": "Go to a food tasting event",
        "date_time": "2024-05-17 17:00:00",
        "status": "completed",
        "next_execute date_time": "2024-05-17 17:00:00",
        "user_id": new ObjectId("663741342683274b18c677aa")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c24"),
        "name": "Explore New Restaurants",
        "description": "Visit new restaurants in town",
        "date_time": "2024-05-21 19:00:00",
        "status": "pending",
        "next_execute date_time": "2024-05-21 19:00:00",
        "user_id": new ObjectId("663741342683274b18c677aa")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c25"),
        "name": "Movie Night",
        "description": "Watch a classic movie",
        "date_time": "2024-05-17 21:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-17 21:00:00",
        "user_id": new ObjectId("663741342683274b18c677ab")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c26"),
        "name": "Visit a Film Festival",
        "description": "Attend a film festival",
        "date_time": "2024-05-23 12:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-23 12:00:00",
        "user_id": new ObjectId("663741342683274b18c677ab")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c27"),
        "name": "Film Discussion",
        "description": "Join a movie discussion group",
        "date_time": "2024-05-22 18:00:00",
        "status": "pending",
        "next_execute_date_time": "2024-05-22 18:00:00",
        "user_id": new ObjectId("663741342683274b18c677ab")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c28"),
        "name": "Plan Next Trip",
        "description": "Plan a summer vacation",
        "date_time": "2024-05-20 10:00:00",
        "status": "pending",
        "next_execute date_time": "2024-05-20 10:00:00",
        "user_id": new ObjectId("663741342683274b18c677ac")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c29"),
        "name": "Book a Flight",
        "description": "Reserve tickets for a flight",
        "date_time": "2024-05-21 15:00:00",
        "status": "pending",
        "next_execute date_time": "2024-05-21 15:00:00",
        "user_id": new ObjectId("663741342683274b18c677ac")
    },
    {
        "_id": new ObjectId("662ff7e536662d902a753c30"),
        "name": "Pack for the Trip",
        "description": "Prepare and pack for the trip",
        "date_time": "2024-05-22 14:00:00",
        "status": "pending",
        "next_execute date_time": "2024-05-22 14:00:00",
        "user_id": new ObjectId("663741342683274b18c677ac")
    }
]


module.exports = tasksSeed
