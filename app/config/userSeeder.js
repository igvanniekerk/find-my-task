const { ObjectId } = require( 'mongodb' );

/**
 * Task defualt Seeder JSON
 */
const usersSeed = [{
    "_id" : new ObjectId("663741342683274b18c677a3"),
    "username" : "tech_guru92",
    "first_name" : "Liam",
    "last_name" : "Smith"
},
{
    "_id" : new ObjectId("663741342683274b18c677a4"),
    "username" : "nature_lover",
    "first_name" : "Olivia",
    "last_name" : "Johnson"
},
{
    "_id" : new ObjectId("663741342683274b18c677a5"),
    "username" : "bookworm33",
    "first_name" : "Noah",
    "last_name" : "Williams"
},
{
    "_id" : new ObjectId("663741342683274b18c677a6"),
    "username" : "fitness_enthusiast",
    "first_name" : "Emma",
    "last_name" : "Jones"
},
{
    "_id" : new ObjectId("663741342683274b18c677a7"),
    "username" : "art_lover",
    "first_name" : "Ava",
    "last_name" : "Brown"
},
{
    "_id" : new ObjectId("663741342683274b18c677a8"),
    "username" : "music_maniac",
    "first_name" : "Sophia",
    "last_name" : "Davis"
},
{
    "_id" : new ObjectId("663741342683274b18c677a9"),
    "username" : "gamer4life",
    "first_name" : "James",
    "last_name" : "Miller"
},
{
    "_id" : new ObjectId("663741342683274b18c677aa"),
    "username" : "foodie_101",
    "first_name" : "Mia",
    "last_name" : "Wilson"
},
{
    "_id" : new ObjectId("663741342683274b18c677ab"),
    "username" : "movie_buff",
    "first_name" : "Lucas",
    "last_name" : "Moore"
},
{
    "_id" : new ObjectId("663741342683274b18c677ac"),
    "username" : "travel_bug",
    "first_name" : "Isabella",
    "last_name" : "Taylor"
}]



module.exports = usersSeed

