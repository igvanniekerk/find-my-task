const winston = require('winston');

/**
 * Creating logger
 */
const logger = winston.createLogger({
  level: 'info', 
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({ filename: 'logs/app.log' }) 
  ],
});

module.exports = logger;
