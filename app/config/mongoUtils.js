require('dotenv').config()
const {MongoClient, ServerApiVersion} = require( 'mongodb' );
const url = process.env.MONGODB_URI
const db_name = process.env.MONGO_DB

class Database {
  db
  constructor() {
    this.client = null; 
    this.db = null
  }

  /**
   * Connects to the db
   * @returns client
   */
  async connect() {
    if (this.client) {
      // If the client already exists, return it
      return this.client;
    }

    try {
      const uri = process.env.MONGODB_URI || 'mongodb://localhost:27017/mydatabase';
      this.client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

      // Connect to the database
      await this.client.connect();

      console.log('Connected to MongoDB');
    } catch (error) {
      console.error('Error connecting to MongoDB:', error);
      throw error;
    }

    return this.client;
  }

  /**
   * get the db connection or connects to
   * db before returning db instance
   * @returns db instance
   */
  async getDb(){
    if(!this.client){
      this.client = await this.connect()
    }    
    this.db = this.client.db('projectdb');
    return this.db
  }

  /**
   * Disconnects the client 
   */
  async disconnect() {
    if (this.client) {
      await this.client.close(); // Close the MongoDB connection
      this.client = null; // Reset the client instance
      console.log('Disconnected from MongoDB');
    }
  }
}

module.exports = new Database();