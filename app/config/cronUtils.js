const moment = require('moment');
const now = moment();
const Tasks = require('../controllers/tasks')

/**
 * Creating logger
 */
const cronJob = async () => {
    const pendingTasks = await Tasks.expired()
    const tasks = pendingTasks.filter((task)=>{
        const givenDate = moment(task?.next_execute_date_time , "YYYY-MM-DD HH:mm:ss");
        if (givenDate.isBefore(now)) {
            return task
        }
    })
    await Tasks.update(tasks.map(({_id})=> _id))
}

module.exports = cronJob;