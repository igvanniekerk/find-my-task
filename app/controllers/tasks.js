const BaseController = require('./BaseController')
const {ObjectId } = require( 'mongodb' );

class TaskController extends BaseController {
  constructor(){
    super('tasks')
  }
  PENDING = 'pending'
  /**
   * Get All Tasks for a user
   * @param { ObjectId } user_id 
   * @returns 
   */
  async getAllForUser(user_id){
    try {
      return await this.db.collection(this.collectionName).find({user_id: new ObjectId(user_id)}).toArray()
    } catch (error) {
      throw Error (`Could not retrieve tasks for user`) 
    }   
  }

  /**
   * Delete User
   * @param { ObjectId } user_id 
   * @param { ObjectId } task_id 
   * @returns 
   */
  async delete(user_id, task_id){
    try {
      return await this.db.collection(this.collectionName).deleteOne({user_id : new ObjectId(user_id), _id: new ObjectId(task_id)})
    } catch (error) {
      throw Error (`Could not delete task for user`) 
    }   
    
  }

  /**
   * Gets all items that is thats pending and 
   * has a next_execute_date_time value
   * @returns Array<Objects>
   */
  async expired(){
    try {
     return await this.db.collection(this.collectionName).find({status : this.PENDING, next_execute_date_time: {$exists: true }  }).toArray()
    } catch (error) {
      throw Error (`Could not run cron tasks`) 
    } 
  }


}
module.exports = new TaskController()

