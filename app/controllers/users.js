const BaseController = require('./BaseController')

class UserController extends BaseController {
  constructor() {
   super('users')
  }
}

module.exports = new UserController()
