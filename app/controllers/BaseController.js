var Database = require('../config/mongoUtils')
const { ObjectId } = require( 'mongodb' );
const logger = require('../config/loggerUtils')

class baseController {

  /**
   * Collection name
   * 
   */
  collectionName

  /**
   * db connection
   * 
   */
  db
  
  /**
   * constructor taking in the collection name
   * creating a connection if it doesnt exists or just 
   * gets the db connection
   * @param {string} collectionName 
   */
  constructor(collectionName) {
    this.collectionName = collectionName
    Database.getDb().then((db) => {
      this.db = db
    })
  }

  /**
   * Creates a new document
   * @param { Object } doc 
   * @returns new doc 
   */
  async create(doc) {
    const newDoc = await this.db.collection(this.collectionName).insertOne(doc)
    if(newDoc?.insertedId){
      return this.db.collection(this.collectionName).findOne({_id: newDoc?.insertedId})
    }
  }

  /**
   * Updates Document
   * @param { ObjectId } id 
   * @param { Object }  update 
   * @returns updated document
   */
  async update(id, update) {
    const set = { $set: update }
    const newDoc = await this.db.collection(this.collectionName).updateOne({ _id: new ObjectId(id) }, set)
    if(newDoc?.matchedCount){
      return this.db.collection(this.collectionName).findOne({_id: new ObjectId(id) })
    }else{
      throw Error (`No ${this.collectionName} was updated`) 
    }
  }

  /**
   * Finds all documents in collection
   * @returns all documents
   */
  async listAll() {
    return await this.db.collection(this.collectionName).find({}).toArray()
  }

  /**
   * Find Document with id
   * @param { ObjectId } id 
   * @returns Document
   */
  async getById(id) {
    const foundDoc =  await this.db.collection(this.collectionName).findOne({ _id: new ObjectId(id) })
    if(foundDoc?.matchedCount){
      return this.db.collection(this.collectionName).findOne({_id: new ObjectId(id) })
    }else{
      throw Error (`No ${this.collectionName} was found`) 
    }
  }

  /**
   * Updates Many Document
   * @param { Array<ObjectId> } ids 
   * @param { Object }  update 
   * @returns updated document
   */
    async update(ids) {
      const set = {$set: {status: 'done'}}
      const filter = { _id : {$in: ids }}
      await this.db.collection(this.collectionName).updateMany(filter, set)
      ids.forEach((id)=>{
        console.log(`Task ${id} is done`)
        logger.info(`Task ${id} is done`)
      })
    }
}

module.exports =  baseController
